﻿using System;

namespace Autosalon.Autos
{
    public class Auto
    {
        public bool IsClean { get; set; }

        public int MaxSpeed { get; set; }

        public bool IsGoodView { get; set; }

        public bool HasGaranty { get; set; }

        public Auto()
        {
            IsClean = false;
            IsGoodView = false;
            HasGaranty = false;
            MaxSpeed = 60;
        }

        public virtual void PresentCharacteristics()
        {
            Console.WriteLine("Standart characteristics of auto");
        }

        public virtual void PresentPrice()
        {
            Console.WriteLine("Medium price of auto");
        }

        public virtual void TestRun()
        {
            Console.WriteLine("YAHOOO!");
        }
    }
}
