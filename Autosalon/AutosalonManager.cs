﻿
namespace Autosalon
{
    public static class AutosalonManager
    {
        private static CabrioletAutosalon exisatedCabrAutosalon = null;

        private static Autosalon exisatedAutosalon = null;

        public static Autosalon GetAutosalon(AutoSalonTypes autosalonType)
        {
            switch (autosalonType)
            {
                case AutoSalonTypes.Cabriolet:
                    return exisatedCabrAutosalon ?? (exisatedCabrAutosalon = new CabrioletAutosalon());
                default:
                    return exisatedAutosalon ?? (exisatedAutosalon = new Autosalon());
            }
        }
    }
}
