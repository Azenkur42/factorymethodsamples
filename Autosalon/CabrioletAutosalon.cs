﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autosalon.Autos;

namespace Autosalon
{
    public class CabrioletAutosalon : Autosalon
    {
        protected override Auto GetAuto()
        {
            return new CabrioletAuto();
        }
    }
}
