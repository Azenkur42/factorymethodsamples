﻿using System;

namespace Autosalon
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to our system");
            var isUserDone = false;
            do
            {
                var autosalon = AutoSalonDispatcher.PreseedClient();
                autosalon.PresentAuto();
                Console.WriteLine("Watch all other presentations or press Q to exit");
                var pressed = Console.ReadLine();
                if (pressed == "Q")
                {
                    isUserDone = true;
                }
            }
            while (!isUserDone);
            Console.ReadLine();
        }
    }
}
