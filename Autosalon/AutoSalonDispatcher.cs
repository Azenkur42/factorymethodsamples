﻿
using System;

namespace Autosalon
{
    public static class AutoSalonDispatcher
    {
        public static Autosalon PreseedClient()
        {
            Console.WriteLine("Please, choose variant of autosalon:");
            var autosalonTypes = Enum.GetNames(
                typeof(AutoSalonTypes));
            for (int i = 0; i < autosalonTypes.Length; i++)
            {
                Console.WriteLine(autosalonTypes[i]);
            }

            var autosalonType = Console.ReadLine();
            while (true)
            {
                AutoSalonTypes clinetType;
                if (Enum.TryParse(autosalonType, true, out clinetType))
                {
                    Console.WriteLine();
                    return AutosalonManager.GetAutosalon(clinetType);
                }
                Console.WriteLine("Please, input correct type");
                autosalonType = Console.ReadLine();
            }
        }
    }
}
