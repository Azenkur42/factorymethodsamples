﻿using Autosalon.Autos;
using System;

namespace Autosalon
{
    public class Autosalon
    {
        private const int NumberOfPreperingBeforeAsk = 3;
        public int autosalonPreperingAutoCount = 0;
        public void PresentAuto()
        {
            var auto = GetAuto();
            this.CleanAuto(auto);
            this.SpeedUpAuto(auto);
            this.TuningAuto(auto);
            this.GetGaranty(auto);
            autosalonPreperingAutoCount++;
            if (autosalonPreperingAutoCount > NumberOfPreperingBeforeAsk)
            {
                this.Asking();
            }

            Console.WriteLine("***********************PRESENTATIONS OF AUTO**********************");
            auto.PresentCharacteristics();
            auto.PresentPrice();
            auto.TestRun();
        }

        protected virtual Auto GetAuto()
        {
            return new Auto();
        }

        private void Asking()
        {
            Console.WriteLine("Are you going to buy our auto, SEREOUSLY?");
        }

        private void CleanAuto(Auto auto)
        {
            auto.IsClean = true;
        }

        private void SpeedUpAuto(Auto auto)
        {
            auto.MaxSpeed = 120;
        }

        private void TuningAuto(Auto auto)
        {
            auto.IsGoodView = true;
        }

        private void GetGaranty(Auto auto)
        {
            auto.HasGaranty = true;
        }
    }
}
