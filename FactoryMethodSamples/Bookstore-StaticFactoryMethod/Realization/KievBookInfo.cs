﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookstore_StaticFactoryMethod.Abstract;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class KievBookInfo : OrderBookInfo
    {
        public KievBookInfo()
        {
            existedBooks = new Dictionary<string, decimal>();
            existedBooks["Faust"] = 370;
            existedBooks["GOFDesignPatterns"] = 120;
            existedBooks["C#AndDotNetPlatform"] = 500;
            existedBooks["Gosudar"] = 400;
            existedBooks["DonKihot"] = 200;
        }

        public override void SuccessOrder()
        {
            Console.WriteLine("Congratulations, you have book from Kiew - Capital of our motherland");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Sorry, Maybe you want to go to some other plases");
        }
    }
}
