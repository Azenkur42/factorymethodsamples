﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookstore_StaticFactoryMethod.Abstract;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class KharkivBookInfo : OrderBookInfo
    {
        public KharkivBookInfo()
        {
            existedBooks = new Dictionary<string, decimal>();
            existedBooks["WarAndPiece"] = 800;
            existedBooks["GOFDesignPatterns"] = 80;
            existedBooks["Cobzar"] = 150;
            existedBooks["CLRViaC#"] = 650;
            existedBooks["RomeoAndJuliette"] = 100;
        }

        public override void SuccessOrder()
        {
            Console.WriteLine("Kharkiv is university and students capita! We have many books");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Go to Balka");
        }
    }
}
