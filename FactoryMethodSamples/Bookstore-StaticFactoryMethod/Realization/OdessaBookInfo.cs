﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookstore_StaticFactoryMethod.Abstract;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class OdessaBookInfo : OrderBookInfo
    {
        public OdessaBookInfo()
        {
            existedBooks = new Dictionary<string, decimal>();
            existedBooks["AnnaKarenina"] = 500;
            existedBooks["GOFDesignPatterns"] = 90;
            existedBooks["Idiot"] = 300;
            existedBooks["CLRViaC#"] = 700;
        }

        public override void SuccessOrder()
        {
            Console.WriteLine("This book from Odess-mama");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Oi, ta vi sho.");
        }
    }
}
