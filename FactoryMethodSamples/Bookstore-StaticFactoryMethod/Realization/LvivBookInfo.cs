﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookstore_StaticFactoryMethod.Abstract;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class LvivBookInfo : OrderBookInfo
    {
        public LvivBookInfo()
        {
            existedBooks = new Dictionary<string, decimal>();
            existedBooks["Faust"] = 300;
            existedBooks["GOFDesignPatterns"] = 100;
            existedBooks["ThreeMushketmens"] = 250;
            existedBooks["Cobzar"] = 140;
        }

        public override void SuccessOrder()
        {
            Console.WriteLine("Lviv is very beutifull city and books from him too");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Strubnu z ratushi yaksho ne cupish!");
        }
    }
}
