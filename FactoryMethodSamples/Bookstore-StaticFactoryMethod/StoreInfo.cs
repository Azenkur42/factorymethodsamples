﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookstore_StaticFactoryMethod.Abstract;
using Bookstore_StaticFactoryMethod.Realization;

namespace Bookstore_StaticFactoryMethod
{
    public static class StoreInfo
    {
        public static OrderBookInfo GetStoreInfoFromCity(string cityName)
        {
            switch(cityName){
                case "Lviw":
                return new LvivBookInfo();
                case "Kharkiv":
                return new KharkivBookInfo();
                case "Kiew":
                return new KievBookInfo();
                case "Odessa":
                return new OdessaBookInfo();
                default:
                return null;
            }
        }
    }
}
