﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore_StaticFactoryMethod.Abstract
{
    public abstract class OrderBookInfo
    {
        protected Dictionary<string, decimal> existedBooks;

        public bool CheckInWarehouse(string bookname)
        {
            return existedBooks.ContainsKey(bookname);
        }

        public decimal ComputePrice(string bookname)
        {
            return existedBooks[bookname];
        }

        public abstract void SuccessOrder();

        public abstract void OrderRejected();
    }
}
