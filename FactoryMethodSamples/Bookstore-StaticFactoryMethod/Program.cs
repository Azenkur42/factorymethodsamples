﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookstore_StaticFactoryMethod.Abstract;

namespace Bookstore_StaticFactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            OrderBookInfo bookInfo;
            Console.WriteLine("Please, choose your city");
            var city = Console.ReadLine();
            bookInfo = StoreInfo.GetStoreInfoFromCity(city);
            if (bookInfo == null)
            {
                Console.WriteLine("Sorry, we havent store in this city");
            }
            else
            {
                Console.WriteLine("Please, choose book");
                var book = Console.ReadLine();
                if (bookInfo.CheckInWarehouse(book))
                {
                    var price = bookInfo.ComputePrice(book);
                    Console.WriteLine("Book price is {0}",price);
                    Console.WriteLine("Do you with this book?");
                    var answer = Console.ReadLine();
                    if (answer == "Yes")
                    {
                        bookInfo.SuccessOrder();
                    }
                    else
                    {
                        bookInfo.OrderRejected();
                    }
                }
                else
                {
                    Console.WriteLine("Sorry, we have not this book! ");
                }
            }
            Console.ReadKey();
        }
    }
}
