﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autosalon
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ivan ordered some standart car and autosalon present it:");
            var autosalon = new Autosalon();
            autosalon.PresentAuto();
            Console.WriteLine("But Aleksey want to order cabriolet and modern autosalon present it:");
            autosalon = new CabrioletAutosalon();
            autosalon.PresentAuto();
            Console.ReadLine();
        }
    }
}
