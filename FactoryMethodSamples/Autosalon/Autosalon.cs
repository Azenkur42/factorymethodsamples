﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autosalon.Autos;

namespace Autosalon
{
    public class Autosalon
    {
        public void PresentAuto()
        {
            var auto = GetAuto();
            Console.WriteLine("Presentations of auto");
            auto.PresentCharacteristics();
            auto.PresentPrice();
            auto.TestRun();
        }

        protected virtual Auto GetAuto()
        {
            return new Auto();
        }
    }
}
