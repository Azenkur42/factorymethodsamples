﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autosalon.Autos
{
    public class Auto
    {
        public virtual void PresentCharacteristics()
        {
            Console.WriteLine("Good characteristics of auto");
        }

        public virtual void PresentPrice()
        {
            Console.WriteLine("Medium price of auto");
        }

        public virtual void TestRun()
        {
            Console.WriteLine("Good! Yahho!");
        }
    }
}
