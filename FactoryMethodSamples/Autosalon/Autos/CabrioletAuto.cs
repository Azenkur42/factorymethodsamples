﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autosalon.Autos
{
    public class CabrioletAuto : Auto
    {
        public override void PresentCharacteristics()
        {
            Console.WriteLine("Good characteristics of cabriolet");
        }

        public override void PresentPrice()
        {
            Console.WriteLine("High price, becose cabrioletes is expensive");
        }
    }
}
