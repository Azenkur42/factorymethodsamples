﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportTicket
{
    public interface ITicketFactory
    {
        ITicketInfo GetEconomTicket();

        ITicketInfo GetBusinessTicket();
    }
}
