﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportTicket.Tickets
{
    public class PlainBusiness : ITicketInfo
    {
        public void PrintInfo()
        {
            Console.WriteLine("This ticket is so expensive, but good flight and comfortable place with good service - its fantastic!");
        }
    }
}
