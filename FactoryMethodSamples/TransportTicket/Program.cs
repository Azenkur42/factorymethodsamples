﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportTicket
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please, enter transport type");
            var TransportType = Console.ReadLine();
            var transportFactory = TicketBuro.GetFactory(TransportType);
            if (transportFactory == null)
            {
                Console.WriteLine("We have not this transport");
            }
            else
            {
                Console.WriteLine("Please, enter ticket type");
                var tickerClass = Console.ReadLine();
                ITicketInfo ticket = null;
                if (tickerClass == "Econom")
                {
                    ticket = transportFactory.GetEconomTicket();
                }
                else if (tickerClass == "Business")
                {
                    ticket = transportFactory.GetBusinessTicket();
                }

                if (ticket != null)
                {
                    ticket.PrintInfo();
                }
                else
                {
                    Console.WriteLine("We have not this tickets");
                }
            }
            Console.ReadKey();
        }
    }
}
