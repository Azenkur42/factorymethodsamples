﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportTicket.Factories;

namespace TransportTicket
{
    public static class TicketBuro
    {
        public static ITicketFactory GetFactory(string type)
        {
            if (type == "Train")
            {
                return new TrainFactory();
            }
            else if (type == "Plain")
            {
                return new PlainFactory();
            }
            return null;
        }
    }
}
