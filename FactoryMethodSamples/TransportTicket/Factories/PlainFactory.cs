﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportTicket.Tickets;

namespace TransportTicket.Factories
{
    class PlainFactory : ITicketFactory
    {
        public ITicketInfo GetEconomTicket()
        {
            return new PlainEconom();
        }

        public ITicketInfo GetBusinessTicket()
        {
            return new PlainBusiness();
        }
    }
}
