﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportTicket.Tickets;

namespace TransportTicket.Factories
{
    public class TrainFactory : ITicketFactory
    {

        public ITicketInfo GetEconomTicket()
        {
            return new TrainEconom();
        }

        public ITicketInfo GetBusinessTicket()
        {
            return new TrainBusiness();
        }
    }
}
