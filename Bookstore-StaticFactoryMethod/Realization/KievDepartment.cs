﻿using Bookstore_StaticFactoryMethod.Abstract;
using System;
using System.Collections.Generic;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class KievDepartment : Department
    {
        public KievDepartment()
        {
            ExistedBooks = new Dictionary<string, decimal>();
            ExistedBooks["Faust"] = 370;
            ExistedBooks["GOFDesignPatterns"] = 120;
            ExistedBooks["C#AndDotNetPlatform"] = 500;
            ExistedBooks["Gosudar"] = 400;
            ExistedBooks["DonKihot"] = 200;
        }

        public override void OrderSuccessed()
        {
            Console.WriteLine("Congratulations, you have book from Kiew - Capital of our motherland");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Sorry, Maybe you want to go to some other plases");
        }
    }
}
