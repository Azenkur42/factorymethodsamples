﻿using Bookstore_StaticFactoryMethod.Abstract;
using System;
using System.Collections.Generic;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class KharkivDepartment : Department
    {
        public KharkivDepartment()
        {
            ExistedBooks = new Dictionary<string, decimal>();
            ExistedBooks["WarAndPiece"] = 800;
            ExistedBooks["GOFDesignPatterns"] = 80;
            ExistedBooks["Cobzar"] = 150;
            ExistedBooks["CLRViaC#"] = 650;
            ExistedBooks["RomeoAndJuliette"] = 100;
        }

        public override void OrderSuccessed()
        {
            Console.WriteLine("Kharkiv is university and students capital! We have many books");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Go to Balka");
        }
    }
}
