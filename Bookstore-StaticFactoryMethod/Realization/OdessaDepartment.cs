﻿using Bookstore_StaticFactoryMethod.Abstract;
using System;
using System.Collections.Generic;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class OdessaDepartment : Department
    {
        public OdessaDepartment()
        {
            ExistedBooks = new Dictionary<string, decimal>();
            ExistedBooks["AnnaKarenina"] = 500;
            ExistedBooks["GOFDesignPatterns"] = 90;
            ExistedBooks["Idiot"] = 300;
            ExistedBooks["CLRViaC#"] = 700;
        }

        public override void OrderSuccessed()
        {
            Console.WriteLine("This book from Odess-mama");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Oi, ta vi sho.");
        }
    }
}
