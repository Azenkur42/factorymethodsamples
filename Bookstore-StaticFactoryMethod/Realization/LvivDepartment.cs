﻿using Bookstore_StaticFactoryMethod.Abstract;
using System;
using System.Collections.Generic;

namespace Bookstore_StaticFactoryMethod.Realization
{
    public class LvivDepartment : Department
    {
        public LvivDepartment()
        {
            ExistedBooks = new Dictionary<string, decimal>();
            ExistedBooks["Faust"] = 300;
            ExistedBooks["GOFDesignPatterns"] = 100;
            ExistedBooks["ThreeMushketmens"] = 250;
            ExistedBooks["Cobzar"] = 140;
        }

        public override void OrderSuccessed()
        {
            Console.WriteLine("Lviv is very beutifull city and books from him too");
        }

        public override void OrderRejected()
        {
            Console.WriteLine("Strubnu z ratushi yaksho ne cupish!");
        }
    }
}
