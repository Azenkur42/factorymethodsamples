﻿using System.Collections.Generic;

namespace Bookstore_StaticFactoryMethod.Abstract
{
    public abstract class Department
    {

        public Dictionary<string, decimal> ExistedBooks { get; set; }

        public bool CheckBookInStore(string bookname)
        {
            return this.ExistedBooks.ContainsKey(bookname);
        }

        public decimal ShowPrice(string bookname)
        {
            return this.ExistedBooks[bookname];
        }

        public abstract void OrderSuccessed();

        public abstract void OrderRejected();
    }
}
