﻿using Bookstore_StaticFactoryMethod.Abstract;
using System;

namespace Bookstore_StaticFactoryMethod
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var city = MainOffice.GetClientLocation();
            Department localDepartment = MainOffice.GetDepartmentInCity(city);
            if (localDepartment != null)
            {
                var book = GetClientDesiredBook();
                HandleOrder(localDepartment, book);
            }
            else
            {
                Console.WriteLine("Sorry, we havent department in this city");
            }
            Console.ReadKey();
        }

        public static void HandleOrder(Department localDepartment, string book)
        {
            if (localDepartment.CheckBookInStore(book))
            {
                var answer = CheckClientAgreement(localDepartment, book);
                if (answer == "Yes")
                {
                    localDepartment.OrderSuccessed();
                }
                else
                {
                    localDepartment.OrderRejected();
                }
            }
            else
            {
                Console.WriteLine("Sorry, we have not this book! ");
            }
        }

        private static string CheckClientAgreement(Department localDepartment, string book)
        {
            var price = localDepartment.ShowPrice(book);
            Console.WriteLine("Book price is {0}", price);
            Console.WriteLine("Do you with this book?");
            var answer = Console.ReadLine();
            return answer;
        }

        public static string GetClientDesiredBook()
        {
            Console.WriteLine("Please, choose book");
            var book = Console.ReadLine();
            return book;
        }
    }
}
