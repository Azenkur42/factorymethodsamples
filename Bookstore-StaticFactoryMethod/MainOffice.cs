﻿using Bookstore_StaticFactoryMethod.Abstract;
using Bookstore_StaticFactoryMethod.Realization;
using System;

namespace Bookstore_StaticFactoryMethod
{
    public abstract class MainOffice
    {
        public static Department GetDepartmentInCity(string cityName)
        {
            switch (cityName)
            {
                case "Lviw":
                    return new LvivDepartment();
                case "Kharkiv":
                    return new KharkivDepartment();
                case "Kiew":
                    return new KievDepartment();
                case "Odessa":
                    return new OdessaDepartment();
                default:
                    return null;
            }
        }

        public static string GetClientLocation()
        {
            Console.WriteLine("Please, choose your city");
            var city = Console.ReadLine();
            return city;
        }
    }
}
