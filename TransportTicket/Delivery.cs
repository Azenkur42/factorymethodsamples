﻿
using Cafes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cafes
{
    public static class Delivery
    {
        private static Dictionary<string, int> allDishes = new Dictionary<string, int>();

        private const int ExitCode = 0;

        public static void PreseedClient(ICafeMenu cafe)
        {
            while (true)
            {
                var clientDishType = PreseedClientChooseDishType();
                if (clientDishType == ExitCode)
                {
                    PrintAllDeliveryList();
                    return;
                }

                DishCategoryHandling((DishTypes)clientDishType, cafe);
            }
        }

        private static void DishCategoryHandling(DishTypes clientDishType, ICafeMenu cafe)
        {
            DishCategory dish = null;
            switch (clientDishType)
            {
                case DishTypes.Pizza:
                    dish = cafe.GetPizza();
                    break;
                case DishTypes.First:
                    dish = cafe.GetFirst();
                    break;
                case DishTypes.Drink:
                    dish = cafe.GetDrink();
                    break;
            }

            if (dish != null)
            {
                var menuPartFromClient = dish.GetDishForClient();
                if (menuPartFromClient.HasValue)
                {
                    allDishes.Add(menuPartFromClient.Value.Key, menuPartFromClient.Value.Value);
                    Console.WriteLine();
                    Console.WriteLine("Thanks for buying.Do you wish anything else?");
                    Console.WriteLine();
                }
            }
        }

        private static void PrintAllDeliveryList()
        {
            Console.WriteLine("Total order:");
            foreach (var dishes in allDishes)
            {
                Console.WriteLine("Dish: {0}. Price: {1} grn", dishes.Key, dishes.Value);
            }
            Console.WriteLine("Total price: {0} grn", allDishes.Values.Sum(x => x));
        }

        private static int PreseedClientChooseDishType()
        {
            Console.WriteLine("Please, choose dish type or enter 0 to start delivery");
            Console.WriteLine("We have categories:");
            var dishCategories = Enum.GetNames(
                typeof(DishTypes));

            for (int i = 0; i < dishCategories.Length; i++)
            {
                Console.WriteLine(dishCategories[i]);
            }
            Console.WriteLine();

            var clientDishPreffered = Console.ReadLine();
            Console.WriteLine();
            while (true)
            {
                DishTypes choosenDishType;
                if (Enum.TryParse(clientDishPreffered, true, out choosenDishType))
                {
                    return (int)choosenDishType;
                }

                if (clientDishPreffered == "0")
                {
                    return ExitCode;
                }

                Console.WriteLine("Incorrect answer.Please, input answer again");
                Console.WriteLine();
                clientDishPreffered = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
