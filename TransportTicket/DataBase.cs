﻿
using Cafes.Enums;
using System.Collections.Generic;
using System.Threading;
using TransportTicket.Enums;

namespace TransportTicket
{
    public static class DataBase
    {

        public static Dictionary<string, int> GetAssortimentFromDb(PresentCafes presentCafe, DishTypes dishType)
        {
            var assortiment = new Dictionary<string, int>();
            if (presentCafe == PresentCafes.Bufet)
            {
                if (dishType == DishTypes.Pizza)
                {
                    assortiment = new Dictionary<string, int>
                              {
                                  {"Firm",34},
                                  {"Meat",37},
                                  {"Madedish",30}
                              };
                }
                else if (dishType == DishTypes.First)
                {
                    assortiment = new Dictionary<string, int>
                              {
                                  {"Soup",15},
                                  {"Okroshka",17}
                              };
                }
                else if (dishType == DishTypes.Drink)
                {
                    assortiment = new Dictionary<string, int>
                              {
                                  {"Milk-shake",12},
                                  {"Lemonade",8}
                              };
                }
            }
            else if (presentCafe == PresentCafes.Trattoria)
            {
                if (dishType == DishTypes.Pizza)
                {
                    assortiment = new Dictionary<string, int>
                              {
                                                {"PizzaFourSeasons",70},
                                                {"SeaPizza",95},
                                                {"PizzaMargarita",50}
                              };
                }
                else if (dishType == DishTypes.First)
                {
                    assortiment = new Dictionary<string, int>
                              {
                                                {"MilanBeef",135},
                                                {"Taljatta",130}
                              };
                }
                else if (dishType == DishTypes.Drink)
                {
                    assortiment = new Dictionary<string, int>
                              {
                                  {"AppleFresh",30},
                                  {"Glyasse",23}
                              };
                }
            }

            Thread.Sleep(1000);
            return assortiment;
        }
    }
}
