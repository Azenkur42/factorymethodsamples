﻿
using System;
using System.Collections.Generic;

namespace Cafes
{
    public abstract class DishCategory
    {
        public Dictionary<string, int> Assortiment { get; set; }
        public abstract KeyValuePair<string, int>? GetDishForClient();

        protected KeyValuePair<string, int>? HandleClientInput()
        {
            if (Assortiment != null)
            {
                Console.WriteLine("We have that assortiment:");
                Console.WriteLine();
                foreach (var dish in this.Assortiment)
                {
                    Console.WriteLine("Dish: {0}. Price: {1} grn", dish.Key, dish.Value);
                }
                Console.WriteLine();
                Console.WriteLine("Enter dish name or 0 to return to main menu");
                Console.WriteLine();
                var choise = Console.ReadLine();
                while (true)
                {
                    if (this.Assortiment.ContainsKey(choise))
                    {
                        return new KeyValuePair<string, int>(choise, Assortiment[choise]);
                    }

                    if (choise == "0")
                    {
                        return null;
                    }
                    Console.WriteLine("Incorect answer!.Please, input it again");
                    Console.WriteLine();
                    choise = Console.ReadLine();
                    Console.WriteLine();
                }
            }

            return null;
        }
    }
}
