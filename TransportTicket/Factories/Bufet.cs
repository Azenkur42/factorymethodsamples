﻿


using Cafes.Dishes;
using Cafes.Enums;

using TransportTicket;
using TransportTicket.Enums;

namespace Cafes.Factories
{
    public class Bufet : ICafeMenu
    {
        public DishCategory GetPizza()
        {
            var pizza = new BufetPizza();
            pizza.Assortiment = DataBase.GetAssortimentFromDb(PresentCafes.Bufet, DishTypes.Pizza);
            return pizza;
        }

        public DishCategory GetFirst()
        {
            var first = new BufetFirst();
            first.Assortiment = DataBase.GetAssortimentFromDb(PresentCafes.Bufet, DishTypes.First);
            return first;
        }

        public DishCategory GetDrink()
        {
            var drink = new BufetDrink();
            drink.Assortiment = DataBase.GetAssortimentFromDb(PresentCafes.Bufet, DishTypes.Drink);
            return drink;
        }
    }
}
