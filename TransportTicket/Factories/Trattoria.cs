﻿

using Cafes.Dishes;
using Cafes.Enums;

using TransportTicket;
using TransportTicket.Enums;

namespace Cafes.Factories
{
    public class Trattoria : ICafeMenu
    {
        public DishCategory GetPizza()
        {
            var pizza = new TrattoriaPizza();
            pizza.Assortiment = DataBase.GetAssortimentFromDb(PresentCafes.Trattoria, DishTypes.Pizza);
            return pizza;
        }

        public DishCategory GetFirst()
        {
            var first = new TrattoriaFirst();
            first.Assortiment = DataBase.GetAssortimentFromDb(PresentCafes.Trattoria, DishTypes.First);
            return first;
        }

        public DishCategory GetDrink()
        {
            var drink = new TrattoriaDrink();
            drink.Assortiment = DataBase.GetAssortimentFromDb(PresentCafes.Trattoria, DishTypes.Drink);
            return drink;
        }
    }
}
