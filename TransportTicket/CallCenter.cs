﻿
using System;

using TransportTicket.Enums;

namespace Cafes
{
    public static class CallCenter
    {
        public static ICafeMenu WaitForClientChooseCafe()
        {
            Console.WriteLine("Welcome to our system");
            Console.WriteLine("Please, enter cafe name.Choose from this variantes:");
            var cafeNames = Enum.GetNames(
                typeof(PresentCafes));
            for (int i = 0; i < cafeNames.Length; i++)
            {
                Console.WriteLine(cafeNames[i]);
            }
            Console.WriteLine();
            var cafeType = Console.ReadLine();
            Console.WriteLine();
            PresentCafes existedeCafe;
            while (true)
            {
                if (Enum.TryParse(cafeType, true, out existedeCafe))
                {
                    return CafeManager.GetCafe(existedeCafe);
                }
                Console.WriteLine("Please, input correct cafe");
                Console.WriteLine();
                cafeType = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
