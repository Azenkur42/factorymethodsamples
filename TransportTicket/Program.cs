﻿using System;

namespace Cafes
{
    class Program
    {
        static void Main(string[] args)
        {
            var cafe = CallCenter.WaitForClientChooseCafe();
            Delivery.PreseedClient(cafe);
            Console.ReadKey();
        }
    }
}
