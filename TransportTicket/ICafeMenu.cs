﻿
namespace Cafes
{
    public interface ICafeMenu
    {
        DishCategory GetPizza();

        DishCategory GetFirst();

        DishCategory GetDrink();
    }
}
