﻿using System;
using System.Collections.Generic;

namespace Cafes.Dishes
{
    class BufetFirst : DishCategory
    {
        public override KeyValuePair<string, int>? GetDishForClient()
        {
            Console.WriteLine("Welcome to bufet! Funny anekdot for you.");
            Console.WriteLine();
            return this.HandleClientInput();
        }
    }
}
