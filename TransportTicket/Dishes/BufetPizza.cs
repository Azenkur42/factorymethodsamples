﻿using System;
using System.Collections.Generic;

namespace Cafes.Dishes
{
    public class BufetPizza : DishCategory
    {
        public override KeyValuePair<string, int>? GetDishForClient()
        {
            Console.WriteLine("Welcome to bufet.We have taste and chip pizza");
            Console.WriteLine();
            return this.HandleClientInput();
        }
    }
}
