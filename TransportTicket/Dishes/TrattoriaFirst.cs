﻿using System;
using System.Collections.Generic;

namespace Cafes.Dishes
{
    public class TrattoriaFirst : DishCategory
    {
        public override KeyValuePair<string, int>? GetDishForClient()
        {
            Console.WriteLine("Welcome to Trattoria. Trattoria - family cafe for every day.What do you want?");
            Console.WriteLine();
            return this.HandleClientInput();
        }
    }
}
