﻿using System;
using System.Collections.Generic;

namespace Cafes.Dishes
{
    public class BufetDrink : DishCategory
    {
        public override KeyValuePair<string, int>? GetDishForClient()
        {
            Console.WriteLine("Welcome to bufet.Good drinks for you");
            Console.WriteLine();
            return this.HandleClientInput();
        }
    }
}
