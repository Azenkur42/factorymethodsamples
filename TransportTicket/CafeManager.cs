﻿
using Cafes.Factories;

using TransportTicket.Enums;

namespace Cafes
{
    public static class CafeManager
    {
        public static ICafeMenu GetCafe(PresentCafes type)
        {
            switch (type)
            {
                case PresentCafes.Bufet:
                    return new Bufet();
                case PresentCafes.Trattoria:
                    return new Trattoria();
                default:
                    return null;

            }
        }
    }
}
